# Bienvenidos al repositorio python_sdk2!
Conceptos avanzados sobre el diseño de software (version control, testing & automatic build management)

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/python_sdk2), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/python_sdk2
```

## Contenidos

```{tableofcontents}
```